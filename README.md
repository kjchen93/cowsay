# Docker example
Test image
```bash
docker run registry.gitlab.com/kjchen93/cowsay
```

## Pull gcc
![](./imgs/pull.jpg)

## Run cowsay
![](./imgs/cowsays.jpg)

## Fun thing to do with docker

### Run a minecraft server
If you love minecraft, you can run your own minecraft server using docker.
How fun is that? 

https://hub.docker.com/search/?q=minecraft&type=image


